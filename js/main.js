/*@constructor*/
class Hamburger {
    constructor(size, stuffing) {
        try {
            if (size && stuffing && arguments.length) {
                this.sizeProp = size;
                this.stuffProp = stuffing;
                this.topping = [];
            } else {
                throw new HamburgerException('Mistake in arguments of Hamburger function');
            }
        } catch (error) {
            console.error(error.name + ": " + error.message);
        }
    }


   set addTopping(topping) {
        try {
            if (!this.topping.includes(topping)) {
                this.topping.push(topping);
            } else {
                throw new HamburgerException('Duplicate topping')
            }
        } catch (error) {
            console.error(error.name + ": " + error.message);
        }
    };


    set removeTopping(topping) {
        try {
            if (this.topping.includes(topping)) {
                this.topping.splice(this.topping.indexOf(topping), 1);
            } else {
                throw new HamburgerException('This toppting dont added');
            }
        } catch (error) {
            console.error(error.name + ": " + error.message);
        }
    };


    get getToppings() {
        const array = [];
        for (let i = 0; i < this.topping.length; i++) {
            array.push(this.topping[i].name)
        }
        return array;
    };


    get getSize() {
        return this.sizeProp.size;
    };


    get getStuffing() {
        return this.stuffProp.name
    };


    calculatePrice() {
        let price = this.sizeProp.price + this.stuffProp.price;
        let toppingsArr = this.topping;
        for (let i = 0; i < toppingsArr.length; i++) {
            price += toppingsArr[i].price;
        }
        return price
    };


    calculateCalories() {
        let calories = this.sizeProp.calories + this.stuffProp.calories;
        let toppingsArr = this.topping;
        for (let i = 0; i < toppingsArr.length; i++) {
            calories += toppingsArr[i].calories;
        }
        return calories
    };


}

function HamburgerException(message) {
    this.message = message;
    this.name = "HamburgerException";
}

Hamburger.SIZE_SMALL = {size: 'small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: 'large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'mayo', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: 'spice', price: 15, calories: 0};


let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping = Hamburger.TOPPING_SPICE;
hamburger.addTopping = Hamburger.TOPPING_MAYO;
// hamburger.addTopping = Hamburger.TOPPING_SPICE;
// hamburger.addTopping = Hamburger.TOPPING_SPICE;
// hamburger.removeTopping = Hamburger.TOPPING_SPICE;
// hamburger.removeTopping = Hamburger.TOPPING_SPICE;
// hamburger.removeTopping = Hamburger.TOPPING_MAYO;
console.log('Toppings:', hamburger.getToppings);
console.log('Size: %s', hamburger.getSize);
console.log('Stuffing: %s', hamburger.getStuffing);
console.log('Price: %d', hamburger.calculatePrice());
console.log('Calories: %d', hamburger.calculateCalories());
console.log("Have %d toppings", hamburger.getToppings.length);
